import unittest
import logging
import os
import shutil

# import matplotlib.pyplot as plt

import svg_tools
import svg_tools.util

test_dir = os.path.dirname(os.path.abspath(__file__))
test_data_dir = os.path.join(test_dir, "test_data")


def polygon_points_iter(file_path: str):
    for polygon in svg_tools.polygon_iter(file_path):
        points_str = polygon.attrib["points"]
        points_pairs = svg_tools._vertices_from_points_str(points_str)
        yield points_pairs



class TestPolygonMod(unittest.TestCase):

    test_svg_file_path = os.path.join(test_data_dir, "Cicle_Vascule.svg")
    test_svg_file_path_copy = os.path.join(test_data_dir, "_Cicle_Vascule.svg")
    cleanup = []
    vertices_list = []

    @classmethod
    def setUpClass(cls):
        cls.cleanup = [cls.test_svg_file_path_copy]
        shutil.copyfile(cls.test_svg_file_path, cls.test_svg_file_path_copy)
        cls.vertices_list = [pair for pair in polygon_points_iter(cls.test_svg_file_path_copy)]

    def test_rotate_if_polygon(self):
        for vert in self.vertices_list:
            rotated = svg_tools.rotate_if_polygon(vert)
        # plt.show()

    # @unittest.skip("")
    def test_rounded_corner_polygon(self):
        for vert in self.vertices_list:
            svg_tools.rounded_corner_polygon(vert, 3)

    # @unittest.skip("")
    def test_jagged_line_polygon(self):
        for vert in self.vertices_list:
            svg_tools.jagged_line_polygon(vert, 0.1)

    # @unittest.skip("")
    def test_added_vertices_polygon(self):
        for vert in self.vertices_list:
            svg_tools.added_vertices_polygon(vert, 1, 3)

    @classmethod
    def tearDownClass(cls):
        for file_path in cls.cleanup:
            os.remove(file_path)


if __name__ == "__main__":


    logging.basicConfig(level=logging.DEBUG)
    logging.getLogger("matplotlib").setLevel(logging.ERROR)
    unittest.main()
