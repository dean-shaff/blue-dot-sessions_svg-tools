import unittest
import os
import shutil

import svg_tools

from . import TestCaseWithTestData_factory


test_dir = os.path.dirname(os.path.abspath(__file__))
test_data_dir = os.path.join(test_dir, "test_data")


class TestPolygonIter(TestCaseWithTestData_factory()):

    test_svg_file_paths = [
        os.path.join(test_data_dir, "Cicle_Vascule.svg"),
        os.path.join(test_data_dir, "Cicle_Vascule.paths.svg")
    ]

    def test_polygon_iter(self):

        for file_path in self.test_svg_file_paths_copy:

            for polygon in svg_tools.polygon_iter(file_path):
                polygon.attrib["fill"] = "none"

            truths = []
            for polygon in svg_tools.polygon_iter(file_path):
                truths.append(polygon.attrib["fill"] == "none")

            self.assertTrue(all(truths))



if __name__ == "__main__":
    unittest.main()
