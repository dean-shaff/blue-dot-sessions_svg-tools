import unittest
import logging
import os
import shutil

import svg_tools

from . import TestCaseWithTestData_factory

test_dir = os.path.dirname(os.path.abspath(__file__))
test_data_dir = os.path.join(test_dir, "test_data")


def dummy(paths):
    return paths

def Dim2FloatType2Dim3FloatType(paths):
    return [[[0.0, 0.0], [1.0, 1.0], [2.0, 2.0]] for idx in range(len(paths))]


class TestModifySVG(TestCaseWithTestData_factory()):

    test_svg_file_paths = [
        os.path.join(test_data_dir, "Cicle_Vascule.svg"),
        os.path.join(test_data_dir, "Cicle_Vascule.paths.svg"),
        os.path.join(test_data_dir, "Arizona_Moon.triangles.svg")
    ]

    def test_modify_svg(self):

        for file_path in self.test_svg_file_paths_copy:
            svg_tools.modify_svg(file_path, dummy)

    # @unittest.skip("")
    def test_modify_svg_chained_fail(self):
        for file_path in self.test_svg_file_paths_copy:
            with self.assertRaises(RuntimeError):
                svg_tools.modify_svg(file_path,
                                     dummy,
                                     Dim2FloatType2Dim3FloatType,
                                     dummy)

    # @unittest.skip("")
    def test_modify_svg_chained(self):
        for file_path in self.test_svg_file_paths_copy:
                svg_tools.modify_svg(file_path,
                                     dummy,
                                     dummy,
                                     Dim2FloatType2Dim3FloatType)




if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG)
    unittest.main()
