import unittest
import os
import shutil


def TestCaseWithTestData_factory():

    class TestCaseWithTestData(unittest.TestCase):

        test_svg_file_paths = []

        def setUp(self):
            self.cleanup = []
            self.test_svg_file_paths_copy = []
            for file_path in self.test_svg_file_paths:
                file_path_copy = f"{os.path.splitext(file_path)[0]}.copy.svg"
                shutil.copyfile(
                    file_path,
                    file_path_copy
                )
                self.test_svg_file_paths_copy.append(file_path_copy)
                self.cleanup.append(file_path_copy)

        def tearDown(self):
            for file_path in self.cleanup:
                os.remove(file_path)


    return TestCaseWithTestData
