import unittest
import logging
import os
import shutil

import svg_tools

from . import TestCaseWithTestData_factory

test_dir = os.path.dirname(os.path.abspath(__file__))
test_data_dir = os.path.join(test_dir, "test_data")


class TestGetRoot(TestCaseWithTestData_factory()):

    test_svg_file_paths = [
        os.path.join(test_data_dir, "Cicle_Vascule.svg"),
        os.path.join(test_data_dir, "Cicle_Vascule.paths.svg"),
        os.path.join(test_data_dir, "Arizona_Moon.triangles.svg")
    ]

    def test_get_root(self):
        for file_path in self.test_svg_file_paths_copy:
            with svg_tools.get_root(file_path) as root:
                self.assertTrue(root.find("g") is not None)


if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG)
    unittest.main()
