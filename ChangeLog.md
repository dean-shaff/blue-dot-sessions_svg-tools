### 0.1.0

- First published version

### 0.1.1

- Update Gitlab CI to only publish on new versions

### 0.2.0

- Added `polygon_iter` function that will easily iterate through polygons in primitive generated SVG file.

### 0.2.1

- Fixed bug where `modify_svg` didn't work with primitive triangles

### 0.3.0

- Significant overhaul of `__main__.py` -- it now uses `argparse` subcommands


### 0.4.0

- allow chaining of polygon modify functions
- modify_svg now takes a function that returns `Dim2FloatType` or `Dim3FloatType` objects
- Can load in paths in addition to polygons

### 0.5.0

- added `get_root` context manager that allows for getting root element of tree without having to import Python XML stuff
- Custom `PrimitiveElement` that I use because I got tired of having to write in the SVG namespace every time I wanted to find an SVG element.
- `modify_svg`, `get_root`, and `polygon_iter` take an optional `output_file_path` keyword argument that can be used to specify where to save file, if we don't want to use `shutil`
